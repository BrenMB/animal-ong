const mongoose = require('mongoose')
const { appreciationSchema } = require('./appreciations.model')

const volunteerSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, 'Email is required']
  },
  pwd: {
    type: String
  },
  name: String,
  country: {
    type: String,
    required: true
  },
  onChargeAnimals: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'animal'
  }],
  appreciations: [appreciationSchema],
})

const volunteerModel = mongoose.model('volunteer', volunteerSchema)
module.exports = volunteerModel